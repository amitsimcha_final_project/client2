﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace loadScrean
{
    public partial class home : Form
    {
        private TcpClient client;
        private IPEndPoint serverEndPoint;
        private NetworkStream clientStream;
        private byte[] bufferGet;
        private byte[] bufferSet;

        private string username;
        private string messageToServer;

        public home()
        {
            bool tryConnect = true;
           
            // התחברות לשרת
            client = new TcpClient();
            serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8826);

            while (tryConnect)
            {
                try
                {
                    client.Connect(serverEndPoint);
                    clientStream = client.GetStream();

                    tryConnect = false;
                    InitializeComponent();

                    // frees the buttons, but not sign in, sign up and quit.
                    joinRoomButton.Enabled = false;
                    createRoomButton.Enabled = false;
                    myStatusButton.Enabled = false;
                    bestScoresButton.Enabled = false;
                }
                catch (Exception)
                {
                    // bulis the messageBox
                    string message = "you do not have a server running.\r\rOk - try again.\rCancel - close client.";
                    MessageBoxButtons buttons = MessageBoxButtons.OKCancel;
                    string caption = "Error Detected";
                    DialogResult result;
                    result = MessageBox.Show(message, caption, buttons);

                    // check witch button was clicked.
                    if (result == DialogResult.Cancel)
                    {
                        tryConnect = false;
                        this.Hide();
                        this.Close();
                    }
                }
            }
        }

        // כפתור ההתנתקות
        private void quitButton_Click(object sender, EventArgs e)
        {
            bufferSet = new ASCIIEncoding().GetBytes("299");
            clientStream.Write(bufferSet, 0, bufferSet.Length);
            clientStream.Flush();

            this.Hide();
            this.Close();
        }

        // כפתור הכניסה
        private void signInButton_Click(object sender, EventArgs e)
        {
            string usernameLen = "";
            string passwordLen = "";
            //check if the textBoxes of username and pass is not empty.
            if (usernameTextBox.Text == "" || passwordTextBox.Text == "")
                errorMessageLable.Text = "?!תגיד לי בעצמך, מה חסר";
            else
            {
                // restart the erreo message.
                errorMessageLable.Text = "";

                // bulid the message
                usernameLen = usernameTextBox.Text.Length.ToString();
                if (usernameLen.Length < 2)
                    usernameLen = "0" + usernameTextBox.Text.Length;
                passwordLen = passwordTextBox.Text.Length.ToString();
                if (passwordLen.Length < 2)
                    passwordLen = "0" + passwordTextBox.Text.Length;
                messageToServer = "200" + usernameLen + usernameTextBox.Text + passwordLen + passwordTextBox.Text;

                //send sign in message "200"
                bufferSet = new ASCIIEncoding().GetBytes(messageToServer);
                clientStream.Write(bufferSet, 0, messageToServer.Length);
                clientStream.Flush();

                // get the answer from the server.
                bufferGet = new byte[4];
                int bytesRead = clientStream.Read(bufferGet, 0, 4);
                string input = new ASCIIEncoding().GetString(bufferGet);

                if (input == "1021")
                {
                    errorMessageLable.Text = "...אופסי דופסי!! המידע שגוי";
                }
                else if (input == "1022")
                {
                    errorMessageLable.Text = "..אמממממ... אתה מחובר כבר";
                }
                else if (input == "1020")
                {
                    // dissapear all the sign in area.
                    username = usernameTextBox.Text;
                    usernameLable.Visible = false;
                    usernameTextBox.Visible = false;
                    passwordLabel.Visible = false;
                    passwordTextBox.Visible = false;
                    signInButton.Visible = false;
                    singOutButton.Visible = true;

                    // free the buttons.
                    joinRoomButton.Enabled = true;
                    createRoomButton.Enabled = true;
                    myStatusButton.Enabled = true;
                    bestScoresButton.Enabled = true;

                    // appear the name of the user name.
                    currUsernameLabel.Text = username;
                    currFrontUsernameLable.Text = username + " שלום לך";
                    currFrontUsernameLable.Visible = true;
                }
                else
                    errorMessageLable.Text = input + " קוד ההודעה הוא";
            }
        }

        // כפתור היציאה
        private void singOutButton_Click(object sender, EventArgs e)
        {
            bufferSet = new ASCIIEncoding().GetBytes("201");
            clientStream.Write(bufferSet, 0, bufferSet.Length);
            clientStream.Flush();

            username = "";
            currUsernameLabel.Text = "";
            currFrontUsernameLable.Text = "";
            usernameTextBox.Text = "";
            passwordTextBox.Text = "";
            usernameLable.Visible = true;
            usernameTextBox.Visible = true;
            passwordLabel.Visible = true;
            passwordTextBox.Visible = true;
            signInButton.Visible = true;
            singOutButton.Visible = false;
        }

        // כפתור ההרשמה
        private void singUpButton_Click(object sender, EventArgs e)
        {
            this.Hide();

            SignUp sign_up_window = new SignUp(clientStream);

            try
            {
                sign_up_window.ShowDialog();
            }
            catch (Exception) { }

            this.Show();
        }
    }
}
