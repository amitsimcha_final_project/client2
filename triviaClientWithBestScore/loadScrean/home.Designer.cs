﻿namespace loadScrean
{
    partial class home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.signInArea = new System.Windows.Forms.TreeView();
            this.usernameTextBox = new System.Windows.Forms.TextBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.signInButton = new System.Windows.Forms.Button();
            this.usernameLable = new System.Windows.Forms.Label();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.singUpButton = new System.Windows.Forms.Button();
            this.joinRoomButton = new System.Windows.Forms.Button();
            this.createRoomButton = new System.Windows.Forms.Button();
            this.myStatusButton = new System.Windows.Forms.Button();
            this.bestScoresButton = new System.Windows.Forms.Button();
            this.quitButton = new System.Windows.Forms.Button();
            this.currUsernameLabel = new System.Windows.Forms.Label();
            this.currFrontUsernameLable = new System.Windows.Forms.Label();
            this.errorMessageLable = new System.Windows.Forms.Label();
            this.singOutButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // signInArea
            // 
            this.signInArea.Location = new System.Drawing.Point(228, 62);
            this.signInArea.Name = "signInArea";
            this.signInArea.Size = new System.Drawing.Size(415, 104);
            this.signInArea.TabIndex = 1;
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Font = new System.Drawing.Font("Guttman Yad-Brush", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.usernameTextBox.Location = new System.Drawing.Point(373, 84);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.Size = new System.Drawing.Size(166, 24);
            this.usernameTextBox.TabIndex = 2;
            this.usernameTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Font = new System.Drawing.Font("Guttman Yad-Brush", 9F);
            this.passwordTextBox.Location = new System.Drawing.Point(373, 111);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.PasswordChar = '*';
            this.passwordTextBox.Size = new System.Drawing.Size(166, 24);
            this.passwordTextBox.TabIndex = 3;
            this.passwordTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // signInButton
            // 
            this.signInButton.BackColor = System.Drawing.SystemColors.HighlightText;
            this.signInButton.Font = new System.Drawing.Font("Guttman Yad-Brush", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.signInButton.Location = new System.Drawing.Point(244, 84);
            this.signInButton.Name = "signInButton";
            this.signInButton.Size = new System.Drawing.Size(123, 51);
            this.signInButton.TabIndex = 4;
            this.signInButton.Text = "כניסה";
            this.signInButton.UseVisualStyleBackColor = false;
            this.signInButton.Click += new System.EventHandler(this.signInButton_Click);
            // 
            // usernameLable
            // 
            this.usernameLable.AutoSize = true;
            this.usernameLable.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.usernameLable.Font = new System.Drawing.Font("Guttman Yad-Brush", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.usernameLable.Location = new System.Drawing.Point(545, 83);
            this.usernameLable.Name = "usernameLable";
            this.usernameLable.Size = new System.Drawing.Size(86, 18);
            this.usernameLable.TabIndex = 5;
            this.usernameLable.Text = ":שם משתמש";
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.passwordLabel.Font = new System.Drawing.Font("Guttman Yad-Brush", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.passwordLabel.Location = new System.Drawing.Point(545, 111);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(56, 18);
            this.passwordLabel.TabIndex = 6;
            this.passwordLabel.Text = ":סיסמא";
            // 
            // singUpButton
            // 
            this.singUpButton.BackColor = System.Drawing.Color.White;
            this.singUpButton.Font = new System.Drawing.Font("Guttman Yad-Brush", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.singUpButton.Location = new System.Drawing.Point(97, 181);
            this.singUpButton.Name = "singUpButton";
            this.singUpButton.Size = new System.Drawing.Size(376, 34);
            this.singUpButton.TabIndex = 7;
            this.singUpButton.Text = "הרשמה";
            this.singUpButton.UseVisualStyleBackColor = false;
            this.singUpButton.Click += new System.EventHandler(this.singUpButton_Click);
            // 
            // joinRoomButton
            // 
            this.joinRoomButton.BackColor = System.Drawing.Color.White;
            this.joinRoomButton.Font = new System.Drawing.Font("Guttman Yad-Brush", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.joinRoomButton.Location = new System.Drawing.Point(377, 221);
            this.joinRoomButton.Name = "joinRoomButton";
            this.joinRoomButton.Size = new System.Drawing.Size(376, 34);
            this.joinRoomButton.TabIndex = 8;
            this.joinRoomButton.Text = "מצא לי חדר";
            this.joinRoomButton.UseVisualStyleBackColor = false;
            // 
            // createRoomButton
            // 
            this.createRoomButton.BackColor = System.Drawing.Color.White;
            this.createRoomButton.Font = new System.Drawing.Font("Guttman Yad-Brush", 12F);
            this.createRoomButton.Location = new System.Drawing.Point(97, 261);
            this.createRoomButton.Name = "createRoomButton";
            this.createRoomButton.Size = new System.Drawing.Size(376, 34);
            this.createRoomButton.TabIndex = 9;
            this.createRoomButton.Text = "צור חדר";
            this.createRoomButton.UseVisualStyleBackColor = false;
            // 
            // myStatusButton
            // 
            this.myStatusButton.BackColor = System.Drawing.Color.White;
            this.myStatusButton.Font = new System.Drawing.Font("Guttman Yad-Brush", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.myStatusButton.Location = new System.Drawing.Point(377, 301);
            this.myStatusButton.Name = "myStatusButton";
            this.myStatusButton.Size = new System.Drawing.Size(376, 34);
            this.myStatusButton.TabIndex = 10;
            this.myStatusButton.Text = "?מ\'מצב";
            this.myStatusButton.UseVisualStyleBackColor = false;
            // 
            // bestScoresButton
            // 
            this.bestScoresButton.BackColor = System.Drawing.Color.White;
            this.bestScoresButton.Font = new System.Drawing.Font("Guttman Yad-Brush", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.bestScoresButton.Location = new System.Drawing.Point(97, 341);
            this.bestScoresButton.Name = "bestScoresButton";
            this.bestScoresButton.Size = new System.Drawing.Size(376, 34);
            this.bestScoresButton.TabIndex = 11;
            this.bestScoresButton.Text = "מי על הפודיום";
            this.bestScoresButton.UseVisualStyleBackColor = false;
            this.bestScoresButton.Click += new System.EventHandler(this.bestScoresButton_Click);
            // 
            // quitButton
            // 
            this.quitButton.BackColor = System.Drawing.Color.White;
            this.quitButton.Font = new System.Drawing.Font("Guttman Yad-Brush", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.quitButton.Location = new System.Drawing.Point(339, 402);
            this.quitButton.Name = "quitButton";
            this.quitButton.Size = new System.Drawing.Size(181, 34);
            this.quitButton.TabIndex = 12;
            this.quitButton.Text = "!נתקו אותי";
            this.quitButton.UseVisualStyleBackColor = false;
            this.quitButton.Click += new System.EventHandler(this.quitButton_Click);
            // 
            // currUsernameLabel
            // 
            this.currUsernameLabel.AutoSize = true;
            this.currUsernameLabel.BackColor = System.Drawing.Color.Transparent;
            this.currUsernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.currUsernameLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.currUsernameLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.currUsernameLabel.Location = new System.Drawing.Point(12, 9);
            this.currUsernameLabel.Name = "currUsernameLabel";
            this.currUsernameLabel.Size = new System.Drawing.Size(0, 39);
            this.currUsernameLabel.TabIndex = 13;
            // 
            // currFrontUsernameLable
            // 
            this.currFrontUsernameLable.AutoSize = true;
            this.currFrontUsernameLable.BackColor = System.Drawing.Color.White;
            this.currFrontUsernameLable.Font = new System.Drawing.Font("Guttman Mantova", 35.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.currFrontUsernameLable.ForeColor = System.Drawing.SystemColors.ControlText;
            this.currFrontUsernameLable.Location = new System.Drawing.Point(234, 82);
            this.currFrontUsernameLable.Name = "currFrontUsernameLable";
            this.currFrontUsernameLable.Size = new System.Drawing.Size(0, 60);
            this.currFrontUsernameLable.TabIndex = 14;
            // 
            // errorMessageLable
            // 
            this.errorMessageLable.AutoSize = true;
            this.errorMessageLable.BackColor = System.Drawing.Color.White;
            this.errorMessageLable.Font = new System.Drawing.Font("Papyrus", 12F);
            this.errorMessageLable.ForeColor = System.Drawing.Color.Red;
            this.errorMessageLable.Location = new System.Drawing.Point(368, 141);
            this.errorMessageLable.Name = "errorMessageLable";
            this.errorMessageLable.Size = new System.Drawing.Size(0, 25);
            this.errorMessageLable.TabIndex = 15;
            // 
            // singOutButton
            // 
            this.singOutButton.BackColor = System.Drawing.Color.White;
            this.singOutButton.Font = new System.Drawing.Font("Guttman Yad-Brush", 12F);
            this.singOutButton.Location = new System.Drawing.Point(97, 181);
            this.singOutButton.Name = "singOutButton";
            this.singOutButton.Size = new System.Drawing.Size(376, 34);
            this.singOutButton.TabIndex = 16;
            this.singOutButton.Text = "!!!תנו לי לצאת";
            this.singOutButton.UseVisualStyleBackColor = false;
            this.singOutButton.Visible = false;
            this.singOutButton.Click += new System.EventHandler(this.singOutButton_Click);
            // 
            // home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::loadScrean.Properties.Resources.binary_brain;
            this.ClientSize = new System.Drawing.Size(889, 459);
            this.Controls.Add(this.singOutButton);
            this.Controls.Add(this.errorMessageLable);
            this.Controls.Add(this.currFrontUsernameLable);
            this.Controls.Add(this.currUsernameLabel);
            this.Controls.Add(this.quitButton);
            this.Controls.Add(this.bestScoresButton);
            this.Controls.Add(this.myStatusButton);
            this.Controls.Add(this.createRoomButton);
            this.Controls.Add(this.joinRoomButton);
            this.Controls.Add(this.singUpButton);
            this.Controls.Add(this.passwordLabel);
            this.Controls.Add(this.usernameLable);
            this.Controls.Add(this.signInButton);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this.usernameTextBox);
            this.Controls.Add(this.signInArea);
            this.Name = "home";
            this.Text = "משחק הטריוויה";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView signInArea;
        private System.Windows.Forms.TextBox usernameTextBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.Button signInButton;
        private System.Windows.Forms.Label usernameLable;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.Button singUpButton;
        private System.Windows.Forms.Button joinRoomButton;
        private System.Windows.Forms.Button createRoomButton;
        private System.Windows.Forms.Button myStatusButton;
        private System.Windows.Forms.Button bestScoresButton;
        private System.Windows.Forms.Button quitButton;
        private System.Windows.Forms.Label currUsernameLabel;
        private System.Windows.Forms.Label currFrontUsernameLable;
        private System.Windows.Forms.Label errorMessageLable;
        private System.Windows.Forms.Button singOutButton;
    }
}