﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Threading;

namespace loadScrean
{
    public partial class bestScore : Form
    {
        public bestScore(string currUsername, string place1Name, string place1Score,
            string place2Name, string place2Score, string place3Name, string place3Score)
        {
            InitializeComponent();
            
            // init the screen.
            currnameLabel.Text = currUsername;
            place1Label.Text = place1Name + " - " + Convert.ToInt32(place1Score);
            place2Label.Text = place2Name + " - " + Convert.ToInt32(place2Score);
            place3Label.Text = place3Name + " - " + Convert.ToInt32(place3Score);
        }
    

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Close();
        }
    }
}
