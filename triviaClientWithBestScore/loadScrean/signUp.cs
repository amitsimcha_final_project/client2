﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace loadScrean
{
    public partial class SignUp : Form
    {
        private byte[] _bufferGet;
        private byte[] _bufferSet;
        NetworkStream _clientStream;

        public SignUp(NetworkStream clientStream)
        {
            _clientStream = clientStream;
            InitializeComponent();
        }


        private void GoBackButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Close();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            string userNameLen = "";
            string passwordLen = "";
            string emailLen = "";

            userNameLen = UserNameTextBox.Text.Length.ToString();
            if (userNameLen.Length < 2)
            {
                userNameLen = "0" + UserNameTextBox.Text.Length;
            }

            passwordLen = PasswordTextBox.Text.Length.ToString();
            if (passwordLen.Length < 2)
            {
                passwordLen = "0" + PasswordTextBox.Text.Length;
            }

            emailLen = EmailTextBox.Text.Length.ToString();
            if (emailLen.Length < 2)
            {
                emailLen = "0" + EmailTextBox.Text.Length;
            }

            // create the message to the server
            string msgToServer = "203" + userNameLen + UserNameTextBox.Text + passwordLen + PasswordTextBox.Text + emailLen + EmailTextBox.Text;

            // send the data about the user to the server
            _bufferSet = new ASCIIEncoding().GetBytes(msgToServer);
            _clientStream.Write(_bufferSet, 0, msgToServer.Length);
            _clientStream.Flush();

            // get a return value from the server
            _bufferGet = new byte[4];
            int bytesRead = _clientStream.Read(_bufferGet, 0, 4);
            string answerFromServer = new ASCIIEncoding().GetString(_bufferGet);

            if(answerFromServer == "1040")
            {
                // say welcome to the new user
                /* var welcomeSound = new System.Media.SoundPlayer();
                 welcomeSound.SoundLocation = loadScrean.Properties.Resources.welcome_sound.ToString();
                 welcomeSound.Play();*/

                UserNameTextBox.Text = "";
                PasswordTextBox.Text = "";
                EmailTextBox.Text = "";

                this.Hide();
                this.Close();
            }
            else if(answerFromServer == "1041")
            {
                ErrorLabel.Text = "Pass illegal";
            }
            else if (answerFromServer == "1042")
            {
                ErrorLabel.Text = "Username is already exists";
            }
            else if (answerFromServer == "1043")
            {
                ErrorLabel.Text = "Username is illegal";
            }
            else if (answerFromServer == "1044")
            {
                ErrorLabel.Text = "Other";
            }
        }

        private void SignUp_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void UserNameTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
